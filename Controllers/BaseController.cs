using System;
using System.Linq.Expressions;
using System.Net;
using KadoIn.Dto;
using KadoIn.Models;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace KadoIn.Controllers
{
    public class BaseController : Controller
    {

        protected ActionResult<WebResponse<T>> CommonJson<T>(T returnValue)
        {
            return Json( WebResponse<T>.SuccessResponse(returnValue) );
        }

        protected User LoggedUser
        {
            get
            {
                try
                {
                    return (User)HttpContext.Items["User"];
                }
                catch (Exception e)
                {
                    return null;
                }
            }
        }
    }
}