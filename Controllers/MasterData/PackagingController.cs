using System.Collections.Generic;
using KadoIn.Constants;
using KadoIn.Dto;
using KadoIn.Helper;
using KadoIn.Models;
using KadoIn.Services;
using Microsoft.AspNetCore.Mvc;

namespace KadoIn.Controllers.MasterData;



[ApiController, Route("api/admin/packagings")]
[Authorize(RestrictedRoles = new UserRole[]{UserRole.Admin })]
public class PackagingController : BaseMasterDataController<Product>
{
    public PackagingController(ProductService service) : base (service)
    {
    }
     

}