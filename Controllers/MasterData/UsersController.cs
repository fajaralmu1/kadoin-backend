
using KadoIn.Constants;
using KadoIn.Helper;
using KadoIn.Models;
using KadoIn.Services;
using Microsoft.AspNetCore.Mvc;

namespace KadoIn.Controllers.MasterData;


[ApiController]
[Route("api/admin/users")]
[Authorize(RestrictedRoles = new UserRole[]{ UserRole.Admin })]
public class UsersController : BaseMasterDataController<User>
{
    public UsersController(UserService userService) : base(userService)
    {
    }
 

}

