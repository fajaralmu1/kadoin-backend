using System.Collections.Generic;
using KadoIn.Constants;
using KadoIn.Dto;
using KadoIn.Helper;
using KadoIn.Models;
using KadoIn.Services;
using Microsoft.AspNetCore.Mvc;

namespace KadoIn.Controllers.MasterData;



[ApiController, Route("api/admin/products")]
[Authorize(RestrictedRoles = new UserRole[]{UserRole.Admin })]
public class ProductsController : BaseMasterDataController<Product>
{
    public ProductsController(ProductService service) : base (service)
    {
    }
     

}