using System.Collections.Generic;
using KadoIn.Dto;
using KadoIn.Models;
using KadoIn.Services;
using Microsoft.AspNetCore.Mvc;

namespace KadoIn.Controllers.MasterData;


public abstract class BaseMasterDataController<T> : BaseController where T : BaseModel
{
    protected readonly BaseMasterDataService<T> _service;

    public BaseMasterDataController(BaseMasterDataService<T> service)
    {
        _service = service;
    }

    [HttpGet]
    public ActionResult<WebResponse<MasterDataResult<T>>> List(
        [FromQuery] int page = 0, 
        [FromQuery] int perPage = 10 )
    {
        IList<T> list = _service.GetList(page, perPage);
        return CommonJson( new MasterDataResult<T>{
            Page        = page,
            PerPage     = perPage,
            Items       = list,
            TotalData   = _service.GetTotalData()
        } );
    }

    [HttpPost]
    public ActionResult<WebResponse<T>> Insert( [FromBody] T model )
    {
        return CommonJson( _service.Add( model ) );
    }

    [HttpPut, Route("{id}")]
    public ActionResult<WebResponse<T>> Update( 
        int id, 
        [FromBody] T item )
    {
        return CommonJson( _service.Update( id, item ) );
    }
    [HttpGet, Route("{id}")]
    public ActionResult<WebResponse<T>> Read( int id )
    {
        _service.Read( id, out T item );
        return CommonJson( item );
    }
    [HttpDelete, Route("{id}")]
    public ActionResult<WebResponse<T>> Delete( int id )
    {
        return CommonJson( _service.Delete( id ) );
    }
}