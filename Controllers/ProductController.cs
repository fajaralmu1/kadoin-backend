
using KadoIn.Models;
using KadoIn.Services;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using KadoIn.Dto;
using KadoIn.Models.GoogleCse;
using KadoIn.ProductInfoLoader;

namespace KadoIn.Controllers
{
    [Route("api/product")]
    [ApiController]
    public class ProductController : BaseController
    {
        private readonly GoogleCseService _googleCseService;
        private readonly ECommerceProductService _eCommerceProductService;
        public ProductController(GoogleCseService googleCseService, ECommerceProductService eCommerceProductService)
        {
            _googleCseService           = googleCseService;
            _eCommerceProductService    = eCommerceProductService;
        }

        [HttpGet, Route("search-ecommerce")]
        public ActionResult<WebResponse<SearchResult>> SearchEcommerce(
            [FromQuery] string key,
            [FromQuery] int startIndex
        )
        {
            SearchResult result = _googleCseService.Search(key, startIndex);
            return CommonJson(result);
        }

        [HttpGet, Route("search-ecommerce-detail")]
        public ActionResult<WebResponse<ProductSpec>> DetailEcommerce(
            [FromQuery] string link
        )
        {
            ProductSpec result = _eCommerceProductService.GetProductSpec( link );
            return CommonJson(result);
        }


    }
}