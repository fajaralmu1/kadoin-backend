
using System.Net;
using KadoIn.Models;
using KadoIn.Services;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using KadoIn.Dto;
using KadoIn.Helper;

namespace KadoIn.Controllers
{
    [Route("api/auth")]
    [ApiController]
    public class AuthController : BaseController
    {
        private readonly UserService _userService;
        public AuthController(UserService userService)
        {
            _userService = userService;
        }
        
        [HttpPost, Route("login")]
        public ActionResult<WebResponse<User>> Login([FromForm] string email,[FromForm] string password)
        {
            User result = _userService.Login(email, password, out string token );
            Response.Headers.Add("access-token", token);
            return CommonJson(result);
        }
        [HttpPost, Route("logout")]
        public ActionResult<WebResponse<string>> Logout()
        {
            return CommonJson("success");
        }

       

        [HttpPost, Route("register")]
        public ActionResult<WebResponse<User>> Register(
            [FromForm] string email,
            [FromForm] string name,
            [FromForm] string displayName,
            [FromForm] string password,
            [FromForm] string phoneNumber
        )
        {
            return CommonJson(_userService.Register(new Models.User {
                Email       = email,
                Name        = name,
                Password    = password,
                DisplayName = displayName,
                PhoneNumber = phoneNumber
            }));
        }

        // authorized

        [HttpGet, Route("profile"), Authorize]
        public ActionResult<WebResponse<User>> GetProfile()
        {
            User user = (User)HttpContext.Items["User"];
            return CommonJson(user);
        }
        
        [HttpPut, Route("profile"), Authorize]
        public ActionResult<WebResponse<User>> UpdateProfile(UserProfileDto profileDto)
        {
            return CommonJson(_userService.UpdateProfile(profileDto, LoggedUser));
        }
    }
}