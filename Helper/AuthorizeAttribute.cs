using System;
using System.Linq;
using KadoIn.Constants;
using KadoIn.Dto;
using KadoIn.Models;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Filters;

namespace KadoIn.Helper
{
    [AttributeUsage(AttributeTargets.Class | AttributeTargets.Method)]
    public class AuthorizeAttribute : Attribute, IAuthorizationFilter
    {
        public UserRole[] RestrictedRoles { get; set; }
        public void OnAuthorization(AuthorizationFilterContext context)
        {
            var user = (User)context.HttpContext.Items["User"];
            if (user == null)
            {
                // not logged in
                context.Result = new JsonResult(WebResponse<string>.ErrorResponse(
                    new UnauthorizedAccessException("Unauthorized")
                )) { 
                    StatusCode = StatusCodes.Status401Unauthorized };
            }
            if (RestrictedRoles.Length > 0 && RestrictedRoles.Any(r => r == user.Role) == false)
            {
                // invalid role
                context.Result = new JsonResult(WebResponse<string>.ErrorResponse(
                    new UnauthorizedAccessException("Forbidden")
                )) { 
                    StatusCode = StatusCodes.Status403Forbidden };   
            }
        }
    }
}