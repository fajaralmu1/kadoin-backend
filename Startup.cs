using System;
using System.Text.Json.Serialization;
using KadoIn.Context;
using KadoIn.Dto;
using KadoIn.Middlewares;
using KadoIn.Services;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;

namespace KadoIn
{
    public class Startup
    {
        public IConfiguration Configuration;
        public Startup(IConfiguration configuration)
        {
            this.Configuration = configuration;
        }

        // This method gets called by the runtime. Use this method to add services to the container.
        // For more information on how to configure your application, visit https://go.microsoft.com/fwlink/?LinkID=398940
        public void ConfigureServices(IServiceCollection services)
        {
            string connectionString = Configuration["AppSetting:ConnectionString"];
            services.AddMvc(options =>
                        options.EnableEndpointRouting = false
                    )
                    .AddJsonOptions(options =>
                        options.JsonSerializerOptions.DefaultIgnoreCondition = JsonIgnoreCondition.WhenWritingNull
                    )
                    .SetCompatibilityVersion(CompatibilityVersion.Version_3_0);

            //DB Context
            services.AddDbContext<AppDBContext>
                    (optionsAction => optionsAction.UseNpgsql(connectionString));

            services.AddSignalR();

            //Services
            services.AddScoped<UserService, UserService>();
            services.AddScoped<ProductService, ProductService>();
            services.AddScoped<PackagingService, PackagingService>();
            services.AddScoped<SettingService, SettingService>();
            services.AddScoped<ECommerceProductService, ECommerceProductService>();
            services.AddSingleton<ApplicationServiceWorker>();
            services.AddSingleton<GoogleCseService>();
            services.AddSwaggerGen();
            // setting
            services.Configure<AppSetting>( Configuration.GetSection( nameof( AppSetting ) ) );

            services.AddHostedService<ApplicationServiceWorker>( o => {
                return o.GetService<ApplicationServiceWorker>();
            });
            
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IWebHostEnvironment env)
        {
            
            app.UseSwagger();
            app.UseSwaggerUI(c =>
            {
                c.SwaggerEndpoint("/swagger/v1/swagger.json", "Rover Simulation V1 API");
            });

            app.UseMiddleware<CustomFilterMiddleware>();
            app.UseMiddleware<JwtMiddleware>();
            // if (env.IsDevelopment())
            // {
            //     app.UseExceptionHandler("/error-local-development");
            // }
            // else
            // {
            app.UseExceptionHandler("/error");
            app.UseRouting();
            // app.UseEndpoints(endpoints => {
            //     endpoints.MapHub<ChatHub>("/chatHub");
            // });
            

            app.UseMvc();
        }
    }

}
