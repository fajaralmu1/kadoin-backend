using KadoIn.Models;

namespace KadoIn.Dto;

public class AppSetting
{
    public string ConnectionString { get; set; }
    public string AuthKey { get; set; }

    public ApplicationProfile DefaultAppProfile { get; set; }
    public User DefaultUser { get; set; }

    public GoogleCse GoogleCse { get; set; }
}

public class GoogleCse 
{
    public string ApiKey { get; set; }
    public string ID { get; set; }
}