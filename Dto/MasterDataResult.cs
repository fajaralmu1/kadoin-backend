
using System.Collections.Generic;
using KadoIn.Models;

namespace KadoIn.Dto;

public class MasterDataResult<T> where T : BaseModel
{
    public int Page { get; set; }
    public int PerPage { get; set; }
    public int TotalData { get; set; }
    public IList<T> Items { get; set; }
}