using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text.Json.Serialization;
using KadoIn.Constants;
using Microsoft.EntityFrameworkCore;

namespace KadoIn.Models
{
    [Table("users")]
    [Index(nameof(Name), IsUnique = true)]
    [Index(nameof(Email), IsUnique = true)]
    public class User : BaseModel
    {
        [Required, Column("name")]
        public string Name { get; set; }
        [Required, Column("display_name")]
        public string DisplayName { get; set; }
        [Required, Column("email")]
        public string Email { get; set; }
        [Required, Column("password"), JsonIgnore]
        public string Password { get; set; }
        [Required, Column("phone_number")]
        public string PhoneNumber { get; set; }

        [JsonConverter(typeof(JsonStringEnumConverter))]
        [Required, Column("role")]
        public UserRole Role { get; set; }

        // [NotMapped]
        // public string Token {get;set;}

    }
}