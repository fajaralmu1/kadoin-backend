using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using KadoIn.Constants;

namespace KadoIn.Models;

[Table("user_orders")]
public class UserOrder : BaseModel
{
    [ForeignKey(nameof(Product)), Column("product_id")]
    public Nullable<int> ProductId { get; set; }
    public Product Product { get; set; }


    [ForeignKey(nameof(Packaging)), Column("packaging_id")]
    public Nullable<int> PackagingId { get; set; }
    public Packaging Packaging { get; set; }

    [Column("thumbnail")]
    public string Thumbnail { get; set; }
    [Column("commerce_name")]
    public string CommerceName { get; set; }
    [Column("target_link")]
    public string TargetLink { get; set; }

    // order info
    [Column("type"), Required]
    public OrderType Type { get; set; } = OrderType.ECommerce;
    [Column("status"), Required]
    public OrderStatus Satus { get; set; } = OrderStatus.Requested;



    // user info

    [ForeignKey(nameof(User)), Column("user_id"), Required]
    public int UserId { get; set; }
    public User User { get; set; }

    [Column("description")]
    public string Description { get; set; }
    [Column("phone_number")]
    public string PhoneNumber { get; set; }
    [Column("address")]
    public string Address { get; set; }
}