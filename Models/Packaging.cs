using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using Microsoft.EntityFrameworkCore;

namespace KadoIn.Models;

[Table("packagings")]
[Index(nameof(Name), IsUnique = true)]
public class Packaging : BaseModel
{
    [Column("name"), Required]
    public string Name { get; set; }
    [Column("image"), Required]
    public string Image { get; set; }
    [Column("description"), Required]
    public string Description { get; set; }

    public override void Update(BaseModel model)
    {
        if (model is Packaging product)
        {
            Name        = product.Name;
            Description = product.Description;
            Image       = product.Image;
            UpdatedDate = System.DateTime.Now;
        }
    }

}