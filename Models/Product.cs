using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using Microsoft.EntityFrameworkCore;

namespace KadoIn.Models
{
    [Table("products")]
    [Index(nameof(Name), IsUnique = true)]
    
    public class Product : BaseModel
    {
        [Column("name"), Required]
        public string Name { get; set; }
        [Column("image"), Required]
        public string Image { get; set; }
        [Column("description"), Required]
        public string Description { get;set; }
        [Column("price"), Required]
        public long Price { get; set; }


        public override void Update(BaseModel model)
        {
            if (model is Product product)
            {
                Name        = product.Name;
                Price       = product.Price;
                Description = product.Description;
                Image       = product.Image;
                UpdatedDate = System.DateTime.Now;
            }
        }
    }
}