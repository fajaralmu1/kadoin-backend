namespace KadoIn.Models.GoogleCse;

public class SearchResult
{
    public Queries Queries { get; set; }
    public SearchItem[] Items { get; set; }
}

public class Queries
{
    public Query[] Request { get; set; }
    public Query[] NextPage { get; set; }
}
public class Query
{
    public string Title { get; set; }
    public string TotalResults { get; set; }
    public string SearchTerms { get; set; }
    public int Count { get; set; }
    public int StartIndex { get; set; }
    public string SearchType { get; set; }
}
public class SearchItem
{
    public string Title { get; set; }
    public string HtmlTitle { get; set; }
    public string Link { get; set; }
    public string DisplayLink { get; set; }
    public string Snippet { get; set; }
    public string HtmlSnippet { get; set; }
    public string Mime { get; set; }
    public string fileFormat { get; set; }
    public ImageResult Image { get; set; }

}

public class ImageResult
{
    public string ContextLink { get; set; }
    public int Height { get; set; }
    public int Width { get; set; }
    public string ThumbnailLink { get; set; }
    public int ThumbnailHeight { get; set; }
    public int ThumbnailWidth { get; set; }
    public int ByteSize { get; set; }
}