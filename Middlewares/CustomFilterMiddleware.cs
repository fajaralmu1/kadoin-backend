using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using KadoIn.Dto;
using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Primitives;
using Newtonsoft.Json;

namespace KadoIn.Middlewares
{
    public class CustomFilterMiddleware
    {
        private readonly RequestDelegate _next;
        private readonly ILogger<CustomFilterMiddleware> _logger;

        public CustomFilterMiddleware(
            RequestDelegate next,
            ILogger<CustomFilterMiddleware> logger
            )
        {
            _next   = next;
            _logger = logger;

            _logger.LogInformation("New Custom Filter Middleware");
        }

        public async Task Invoke(HttpContext context /* other dependencies */)
        {
            _logger.LogInformation($"{context.Request.Method} : {context.Request.Path}{context.Request.QueryString}");
            LogHeaders(context);
            PopulateResponseForCors(context.Response);
            
            if (!context.Request.Method.ToLower().Equals("options"))
            {
                await _next(context);
            }

            _logger.LogInformation("================ COMPLETED:" + context.Response.StatusCode);

        }

        private void LogHeaders(HttpContext context)
        {
            _logger.LogInformation("========= HEADERS =========");
            foreach(KeyValuePair<string, StringValues> header in context.Request.Headers)
            {
                _logger.LogInformation($"{ header.Key } : { header.Value }");
            }
        }

        public static void PopulateResponseForCors(HttpResponse response)
        {
            response.Headers.Add("Access-Control-Allow-Origin", "*");
            response.Headers.Add("Access-Control-Expose-Headers", "access-token, requestid");
            response.Headers.Add("Access-Control-Allow-Credentials", "true");
            response.Headers.Add("Access-Control-Allow-Methods", "POST, PUT, GET, OPTIONS, DELETE");
            response.Headers.Add("Access-Control-Max-Age", "3600");
            response.Headers.Add("Access-Control-Allow-Headers",
                    "Content-Type, Accept, X-Requested-With, Authorization, requestid, access-token");
            response.StatusCode = StatusCodes.Status200OK;
        }
    }
}