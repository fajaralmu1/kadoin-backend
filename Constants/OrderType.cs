namespace KadoIn.Constants;

public enum OrderType
{
    ECommerce,
    Internal
}