namespace KadoIn.Constants;

public enum OrderStatus
{
    Requested,
    Processed,
    Delivered,
    Canceled,
    NotProcessed,
}