namespace KadoIn.Constants;

public enum UserRole
{
    User, Admin
}