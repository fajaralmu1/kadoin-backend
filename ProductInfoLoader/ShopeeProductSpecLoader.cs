
using System.Net.Http;
using System.Text.Json;
using System.Text.Json.Serialization;
using System.Threading.Tasks;

namespace KadoIn.ProductInfoLoader;


public class ShopeeProductSpecLoader : IProductSpecLoader
{
    private readonly string _url;
    static readonly HttpClient httpClient = new HttpClient();
    public ShopeeProductSpecLoader( string url )
    {
        _url = url.Split("?")[0];
    }

    string GetDetailUrl( string shopId, string itemId )
    {
        return $"https://shopee.co.id/api/v4/item/get?itemid={itemId}&shopid={shopId}";
    }

    public ProductSpec Load()
    {
        string[] splitByDots    = _url.Split('.');
        string shopId           = splitByDots[splitByDots.Length - 2];
        string itemId           = splitByDots[splitByDots.Length - 1];
        Task<string> task       = httpClient.GetStringAsync( GetDetailUrl( shopId, itemId ) );
        task.Wait();
        SPProductInfo info = JsonSerializer.Deserialize<SPProductInfo>( task.Result );
        return new ProductSpec() {
            Name    = info.Data.Name,
            Price   = info.Data.Price / 1000,
            Image   = $"https://cf.shopee.co.id/file/{info.Data.Image}"
        };
    }
}

class SPProductInfo
{
    [JsonPropertyName("data")]
    public SPData Data { get; set; }
}

class SPData 
{
    [JsonPropertyName("name")]
    public string Name { get; set; }
    [JsonPropertyName("image")]
    public string Image { get; set; }
    [JsonPropertyName("price")]
    public long Price { get; set; }
}