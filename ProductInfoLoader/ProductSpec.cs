namespace KadoIn.ProductInfoLoader;



public class ProductSpec
{
    public string Name { get; set; }
    public long Price { get; set; }
    public string Image { get; set; }
}