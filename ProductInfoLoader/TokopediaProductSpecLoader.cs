
using System.IO;
using System.Net.Http;
using System.Threading.Tasks;
using System.Xml;

namespace KadoIn.ProductInfoLoader;

class TokopediaProductSpecLoader : IProductSpecLoader
{
    static readonly HttpClient httpClient = new HttpClient();
    private readonly string _uri;
    public TokopediaProductSpecLoader( string uri )
    {
        _uri = uri;
    }
    public ProductSpec Load()
    {
        Task<string> task = httpClient.GetStringAsync( _uri );
        task.Wait();
        string upperSeparator = "<meta data-rh=\"true\" property=\"og:site_name\" content=\"Tokopedia\"/>";
        string lowerSeparator = "<meta data-rh=\"true\" name=\"robots\" content=\"none\"/>";
        // Console.WriteLine("task.Result.Split(upperSeparator): " +  task.Result.Split(upperSeparator)[0] );
        
        string metaElements         = task.Result.Split(upperSeparator)[1].Split(lowerSeparator)[0];
        StringReader stringReader   = new StringReader( "<root>" + metaElements + "</root>" );
        XmlDocument document        = new XmlDocument();

        document.Load(stringReader);
        
        XmlNodeList elements        = document.ChildNodes[0].ChildNodes;
        ProductSpec spec            = new ProductSpec();

        foreach (XmlNode item in elements)
        {
            if (item.Attributes?["property"]?.Value == "product:price:amount")
            {
                spec.Price = long.Parse(item.Attributes["content"].Value);
                continue;
            }
            if (item.Attributes?["name"]?.Value == "twitter:title")
            {
                spec.Name = item.Attributes["content"].Value;
                continue;
            }
            if (item.Attributes?["property"]?.Value == "og:image")
            {
                spec.Image = item.Attributes["content"].Value;
                continue;
            }
        }

        return spec;
    }
}