using System;

namespace KadoIn.ProductInfoLoader;

public interface IProductSpecLoader
{
    ProductSpec Load();
    static IProductSpecLoader GetLoader( string uri )
    {
        Uri myUri = new Uri(uri);   
        switch (myUri.Host)
        {
             case "www.tokopedia.com":
             case "tokopedia.com":
                return new TokopediaProductSpecLoader( uri );
             case "www.shopee.co.id":
             case "shopee.co.id":
                return new ShopeeProductSpecLoader( uri );
             case "www.bukalapak.com":
             case "bukalapak.com":
                return new BukalapakProductSpecLoader( uri );

        }
        throw new ArgumentException( $"Host is not known: { myUri.Host }" );
    }
}

 