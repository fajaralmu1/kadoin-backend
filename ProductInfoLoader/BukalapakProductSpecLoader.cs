
using System.Linq;
using System.Net.Http;
using System.Text.Json;
using System.Text.Json.Serialization;
using System.Threading.Tasks;
using System.Web;
using System.Xml;

namespace KadoIn.ProductInfoLoader;

class BukalapakProductSpecLoader : IProductSpecLoader
{
    static readonly HttpClient httpClient = new HttpClient();
    private readonly string _uri;
    public BukalapakProductSpecLoader( string uri )
    {
        httpClient.DefaultRequestHeaders.UserAgent.ParseAdd( "C# Net (Windows NT 10.0; Win64; x64; rv:97.0)" );
        _uri = uri;
    }
    public ProductSpec Load()
    {
        Task<string> task = httpClient.GetStringAsync( _uri );
        task.Wait();
        string[] splitByLine    = task.Result.Split("\n"); 
        string scriptLine       = splitByLine.Where(line => line.Trim().StartsWith("<script id=initial-pdp>")).First();
         
         
        string pureScript = scriptLine.Trim()
            .Split("<script id=initial-pdp>window.__INITIAL_PDP__=\"")[1]
            .Split("\"</script>")[0];

        string decodedScript    = HttpUtility.UrlDecode(pureScript);
        BLProductInfo info      = JsonSerializer.Deserialize<BLProductInfo>( decodedScript );

        return new ProductSpec()
        {
            Name    = info.Product.Name,
            Price   = info.Product.Price,
            Image   = info.Product.Images.LargeUrls[0]
        };
    }
    
}

class BLProductInfo
{
    [JsonPropertyName("product")]
    public BLProduct Product { get; set; }
}

class BLProduct
{
    [JsonPropertyName("name")]
    public string Name { get; set; }
    [JsonPropertyName("price")]
    public long Price { get; set; }
    [JsonPropertyName("images")]
    public BLProductImages Images { get; set; }
}

class BLProductImages
{
    [JsonPropertyName("large_urls")]
    public string[] LargeUrls { get; set; }
    [JsonPropertyName("smalls_urls")]
    public string[] SmallUrls { get; set; }
}