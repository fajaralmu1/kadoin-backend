using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Logging;

namespace KadoIn
{
    public class Program
    {
        public static void Main(string[] args)
        {
           CreateHostBuilder(null).Build().Run();
        }

        public static IHostBuilder CreateHostBuilder(string[] args) =>
            Host.CreateDefaultBuilder(args)
                .ConfigureLogging(logging =>
                {
                    logging.ClearProviders();
                    
                    logging.AddSimpleConsole(configure=>{
                        configure.ColorBehavior     = Microsoft.Extensions.Logging.Console.LoggerColorBehavior.Enabled;
                        configure.SingleLine        = true;
                        configure.UseUtcTimestamp   = true;
                        configure.TimestampFormat   = "yyyy-MM-dd HH:mm:ss";
                    });
                })
                .ConfigureWebHostDefaults(webBuilder =>
                {
                    webBuilder.UseStartup<Startup>();
                    webBuilder.UseUrls("http://*:5000");
                });
    }
}
