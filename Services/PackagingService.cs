using KadoIn.Context;
using KadoIn.Models;
using Microsoft.EntityFrameworkCore;

namespace KadoIn.Services;

public class PackagingService : BaseMasterDataService<Packaging>
{
    public PackagingService(AppDBContext dBContext): base(dBContext)
    {
        
    }

    protected override DbSet<Packaging> Items => _context.Packagings;
}