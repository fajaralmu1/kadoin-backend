using KadoIn.Context;
using KadoIn.Models;
using Microsoft.EntityFrameworkCore;

namespace KadoIn.Services;

public class ProductService : BaseMasterDataService<Product>
{
    public ProductService(AppDBContext dBContext): base(dBContext)
    {
        
    }

    protected override DbSet<Product> Items => _context.Products;
}