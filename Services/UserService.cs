using System;
using System.Collections.Generic;
using System.Linq;
using KadoIn.Models;
using KadoIn.Context;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.Primitives;
using System.Security.Authentication;
using System.IdentityModel.Tokens.Jwt;
using System.Security.Claims;
using System.Text;
using Microsoft.Extensions.Configuration;
using Microsoft.IdentityModel.Tokens;
using Microsoft.EntityFrameworkCore;
using KadoIn.Dto;
using Microsoft.Extensions.Options;

namespace KadoIn.Services
{
    public class UserService : BaseMasterDataService<User>
    {
        private readonly AppSetting _setting;

        protected override DbSet<User> Items => _context.Users;

        public UserService(AppDBContext context, IOptions<AppSetting> configuration) :base(context)
        { 
            _setting = configuration.Value;
        }

        internal IEnumerable<User> getUsers()
        {
            return _context.Users.ToList<User>();
        }

        internal User Register(User requestUser)
        {
            User user = new User()
            {
                Role        = Constants.UserRole.User,
                ID          = 0,
                Email       = requestUser.Email,
                Name        = requestUser.Name,
                DisplayName = requestUser.DisplayName,
                Password    = HashPassword( requestUser.Password ),
                PhoneNumber = requestUser.PhoneNumber
            };

            Add(user);
            user.Password = null;
            return user;
        }

        internal List<User> GetByNameLike(User loggedUser, string name)
        {
            return Items.Where(u => u.ID != loggedUser.ID && u.Name.ToLower().Contains(name.ToLower())).ToList();
        }

        internal User UpdateProfile(UserProfileDto profileDto, User user)
        {
            if (profileDto.Email != null) {
                user.Email = profileDto.Email;
            }
            if (profileDto.Name != null) {
                user.Name = profileDto.Name;
            }
            if (profileDto.Password != null) {
                user.Password = HashPassword(profileDto.Password);
            }
            Items.Update(user);
            _context.SaveChanges();
            return user;
        }

        public static string HashPassword(string value)
        {
            return BCrypt.Net.BCrypt.HashPassword(value);
        }

        internal User Login(string email, string password, out string token)
        {
            token = null;
            if (!_context.Users.Any(u => u.Email == email)){
                throw new InvalidCredentialException($"Email :{email} not found");
            }
            User user = _context.Users.First(u => u.Email == email);
            if (!BCrypt.Net.BCrypt.Verify(password,  user.Password)) {
                throw new InvalidCredentialException("Password incorrect");
            }
            token = GenerateJwtToken(user);
            return user;
        }

        internal User GetByEmail(string email)
        {
            return _context.Users.Where(u=>u.Email == email).FirstOrDefault();
        }

        public string GenerateJwtToken(User user)
        {
            // generate token that is valid for 7 days
            var tokenHandler    = new JwtSecurityTokenHandler();
            var key             = Encoding.ASCII.GetBytes( _setting.AuthKey );
            var tokenDescriptor = new SecurityTokenDescriptor
            {
                Subject             = new ClaimsIdentity(new[] { new Claim("email", user.Email) }),
                Expires             = DateTime.UtcNow.AddDays(7),
                SigningCredentials  = new SigningCredentials(new SymmetricSecurityKey(key), SecurityAlgorithms.HmacSha256Signature)
            };
            var token = tokenHandler.CreateToken(tokenDescriptor);
            return tokenHandler.WriteToken(token);
        }
    }
}