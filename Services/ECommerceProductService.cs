using KadoIn.Models.GoogleCse;
using KadoIn.ProductInfoLoader;

namespace KadoIn.Services;

public class ECommerceProductService
{
    public ProductSpec GetProductSpec( string link )
    {
        IProductSpecLoader loader = IProductSpecLoader.GetLoader( link );
        return loader.Load();
    }
}