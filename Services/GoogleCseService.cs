using System.Net.Http;
using System.Text.Json;
using System.Threading.Tasks;
using KadoIn.Dto;
using KadoIn.Models.GoogleCse;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;

namespace KadoIn.Services;

public class GoogleCseService
{
    static JsonSerializerOptions JsonOption = new JsonSerializerOptions()
    {
        PropertyNamingPolicy = JsonNamingPolicy.CamelCase
    };
    private AppSetting _setting;
    private ILogger<GoogleCseService> _logger;
    public GoogleCseService( IOptions<AppSetting> setting, ILogger<GoogleCseService> logger )
    {
        _setting = setting.Value;
        _logger  = logger;
    }
    

    string SearchUrl(string key, int startIndex)
    {
        return $"https://www.googleapis.com/customsearch/v1?" + 
               $"key={ _setting.GoogleCse.ApiKey }" +
               $"&cx={ _setting.GoogleCse.ID }" + 
               $"&q={ key }" + 
               $"&searchType=image" + 
               $"&start={ startIndex }";
    }

    public SearchResult Search(string key, int startIndex)
    {
        HttpClient client = new HttpClient();
        string url        = SearchUrl(key, startIndex);
        // string url        = "http://localhost/mockgooglecustomsearch/result1.json"; // SearchUrl(key, startIndex);
        Task<string> task = client.GetStringAsync(url);
        task.Wait();

        _logger.LogInformation($"Search url: " + url);

        string json = task.Result;

        return JsonSerializer.Deserialize<SearchResult>( json, JsonOption );
    }
 
}