using System.Threading;
using System.Threading.Tasks;
using KadoIn.Context;
using KadoIn.Dto;
using KadoIn.Models;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;
using System.Linq;

namespace KadoIn.Services;

public class ApplicationServiceWorker : IHostedService
{
    private readonly AppDBContext _context;
    private readonly AppSetting _setting;
    private readonly ILogger<ApplicationServiceWorker> _logger;
    public ApplicationServiceWorker( 
        IServiceScopeFactory scopeFactory, 
        IOptions<AppSetting> setting,
        ILogger<ApplicationServiceWorker> logger )
    {
        _context    = scopeFactory.CreateScope().ServiceProvider.GetRequiredService<AppDBContext>();
        _setting    = setting.Value;
        _logger     = logger;
    }
    public Task StartAsync(CancellationToken cancellationToken)
    {
        _logger.LogInformation( "Service worker started" );
        CheckDefaultRecords();
        return Task.CompletedTask;
    }

    private void CheckDefaultRecords()
    {
        CheckAdmin();
        CheckAppProfile();
    }

    private void CheckAdmin()
    {
        User defaultUser    = _setting.DefaultUser;
        bool found          = _context.Users.Any( u => u.Name == defaultUser.Name && u.Role == defaultUser.Role );
        if (found)
        {
            return;
        }
        _logger.LogInformation( "Adding default user" );
        defaultUser.Password = UserService.HashPassword( defaultUser.Password );
        _context.Users.Add( defaultUser );
        _context.SaveChanges();
    }

    private void CheckAppProfile()
    {
        ApplicationProfile profile  = _setting.DefaultAppProfile;
        bool found                  = _context.ApplicationProfiles.Any();
        if (found)
        {
            return;
        }
        _logger.LogInformation( "Add default app profile: " + profile.Description );
        _context.ApplicationProfiles.Add( profile );
        _context.SaveChanges();
    }

    public Task StopAsync(CancellationToken cancellationToken)
    {
        _logger.LogInformation( "Service worker stopped" );

        return Task.CompletedTask;
    }
}