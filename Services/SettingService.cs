using System;
using System.Linq;
using KadoIn.Context;
using KadoIn.Dto;
using KadoIn.Models;
using Microsoft.AspNetCore.SignalR;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Internal;
using Microsoft.Extensions.Options;

namespace KadoIn.Services
{
    public class SettingService
    {
        private readonly AppSetting _setting;
        private readonly AppDBContext _context;
        public SettingService(AppDBContext context, IOptions<AppSetting> setting)
        {
            _setting = setting.Value;
            _context = context;
        }

        private static readonly Random _random = new Random();
        protected DbSet<ApplicationProfile> Items => _context.ApplicationProfiles;

        internal ApplicationProfile GetProfile()
        {
            ApplicationProfile model = Items.FirstOrDefault();
            
            return model;
        }

        public AppSetting GetSetting()
        {
            return _setting;
        }
    }
}