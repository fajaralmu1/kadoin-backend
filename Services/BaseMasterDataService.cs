using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Threading.Tasks;
using KadoIn.Context;
using KadoIn.Helper;
using KadoIn.Models;
using Microsoft.EntityFrameworkCore;

namespace KadoIn.Services
{
    public abstract class BaseMasterDataService<T> where T : BaseModel
    {
        protected AppDBContext _context;
        protected abstract DbSet<T> Items { get; }

        public BaseMasterDataService(AppDBContext context)
        {
            _context = context;
        }

        public T Add(T model)
        {
            Items.Add(model);
            _context.SaveChanges();
            return model;
        }

        protected IList<T> GetList(Expression<Func<T, bool>> where)
        {
            return Items.Where(where).ToList();
        }

        internal int GetTotalData()
        {
            return Items.Count();
        }

        protected IQueryable<T> GetQuery(Expression<Func<T, bool>> where)
        {
            return Items.Where(where);
        }

        public IList<T> GetList(int page, int perPage)
        {
            return Items.Skip(page * perPage).Take(perPage).ToList();
        }

        public T Delete(int id, Expression<Func<T, bool>> where = null)
        {
            IQueryable<T> queryable = Items.Where(m => m.ID == id);
            if (where != null)
            {
                queryable.Where(where);
            }
            T model = queryable.FirstOrDefault();
            if (null == model)
                return null;
            Items.Remove(model);
            _context.SaveChanges();
            return model;
        }

        public bool Read(int id, out T model)
        {
            model = Items.Where(model => model.ID == id).FirstOrDefault();
            return model != null;
        }

        public T Update(int id, T model)
        {
            if (!Read(id, out T oldModel))
            {
                throw new Exception("data not found");
            }
            oldModel.Update(model);
            _context.SaveChanges();
            return oldModel;
        }

        protected static IEqualityComparer<CT> CreateEqualityComparer<CT>(
           Func<CT, int> getHashCode,
           Func<CT, CT, bool> equals
           )
        {
            return new DelegatedEqualityComparer<CT>(getHashCode, equals);
        }
    }
}